[[_TOC_]]

## Helpful commands

### Launching multiple ROS nodes locally

```bash
roslaunch PACKAGE_NAME FILE.launch
```

## Relevant links

### ROS Control

- **ros_control**: http://wiki.ros.org/ros_control
- **URDF Transmissions**: https://wiki.ros.org/urdf/XML/Transmission

### Gazebo and ROS Control

- **Tutorial: ROS Control**: http://gazebosim.org/tutorials/?tut=ros_control
